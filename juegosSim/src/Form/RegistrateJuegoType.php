<?php

namespace App\Form;

use App\Entity\Juego;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrateJuegoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('titulo', TextType::class)
            ->add('descripcion', TextType::class)
        ;
        //si la accion es editJuego entonces se podrá cambiar el estado del juego seleccionando el que queramos
        if ($options['accion'] == 'editJuego')
        {
            $builder->add('estado', ChoiceType::class, array(
                    'choices' => array(
                    'En Desarrollo' => 'En Desarrollo',
                    'Activo' => 'Activo',
                    'Inactivo' => 'Inactivo'
                )
            ))
            ;
        }
        $builder
            ->add('save', SubmitType::class,['label' => 'Guardar']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Juego::class,
            'accion' => 'Crear juego',
        ]);
    }
}
