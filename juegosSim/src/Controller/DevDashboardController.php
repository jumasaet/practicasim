<?php

namespace App\Controller;

use App\Entity\Juego;
use App\Entity\User;
use App\Repository\JuegoRepository;
use App\Repository\UserRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class DevDashboardController extends AbstractController
{
    #[Route('/dev/dashboard', name: 'app_dev_dashboard')]
    public function index( JuegoRepository $juegosRep, AuthenticationUtils $authenticationUtils,UserRepository $usRep): Response
    {
        $juegos = $juegosRep->findAll();
        $lasUser = $authenticationUtils->getLastUsername();
        $user = $usRep->findOneByUserMail($lasUser);
        return $this->render('dev_dashboard/index.html.twig', [
            'juegos' => $juegos,'usuario' => $user,
        ]);
    }

    #[Route('/dev/delete/{id}', name: 'dev_juego_delete')]
    public function delete(Juego $juego, ManagerRegistry $doctrine): Response
    {
        $em = $doctrine->getManager();
        $em->remove($juego);
        $em->flush();
        $this->addFlash("success", "Juego eliminado exitosamente");
        return $this->redirectToRoute('app_dev_dashboard');
    }

}
