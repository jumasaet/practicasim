<?php

namespace App\Controller;

use App\Entity\Juego;
use App\Form\RegistrateJuegoType;
use App\Repository\JuegoRepository;
use App\Repository\UserRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class RegisterJuegoController extends AbstractController
{
    #[Route('/register/juego', name: 'app_register_juego')]
    public function index(Request $request , ManagerRegistry $doctrine, AuthenticationUtils $authenticationUtils,UserRepository $userRep): Response
    {
        //se le asigna ese valor ya que es el rol por defecto y en el constructor que le hicimos pusimos que reciba un rol
        $lasUser = $authenticationUtils->getLastUsername();
        $idLast = ($userRep->findOneBy(["email"=>$lasUser]))->getId();
        $juego = new Juego("En Desarrollo",);
        $form = $this->createForm(RegistrateJuegoType::class,$juego);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $juego = $form->getData();
            $juego->setIdUser($idLast);

            $em = $doctrine->getManager();
            $em->persist($juego);
            $em->flush();
            
            return $this->redirectToRoute('app_dev_dashboard');

        }
        return $this->render("register_juego/index.html.twig",[
            'formulario' => $form->createView(),'usuario' => $lasUser,
        ]);
    }

    #[Route('/dev/edit/{id}', name: 'dev_juego_edit')]
    public function edit(Juego $juego, Request $request, JuegoRepository $juegosRep , UserRepository $userRep, AuthenticationUtils $authenticationUtils): Response
    {
        $msj = "";
        $estado = "";
        $usuariox = $authenticationUtils->getLastUsername();
        $user = $userRep->findOneBy(["email" => $usuariox]);
        
        //iniciamos un FORMulario de tipo RegisterJuegoType, mandamos el juego con el que tenemos el formulario
        //y le mandamos la 'accion' de editJuego para que se nos permita seleccionar el estado del juego
        $form = $this->createForm(RegistrateJuegoType::class,$juego, array('accion' =>'editJuego'));
        $form ->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) 
        {
            $juego = $form->getData();
            if ($juego->getEstado() =="Activo")
            {
                //aqui pedimos que nos devuelva la cantidad de juegos que tiene el usuario con estado activo
                $totalJuegos = $juegosRep->totalByUserIdAndEstado($user->getId(),"Activo");
                if($totalJuegos != null && $totalJuegos >=2)
                {
                    $msj = "Error: No se puede tener mas de dos juegos activos para test";
                    $estado = "error";
                }
            }elseif($juego->getEstado() == "En Desarrollo")
            {
                $msj = "Error: No se puede cambiar el estado debido a que el juego esta en desarrollo";
                $estado = "error";
            }

            if($estado != "error")
            {
                $msj = "Juego actualizado exitosamente";
                $estado = "success";
                $juegosRep->add($juego);
            }
            $this->addFlash($estado, $msj);
            return $this->redirectToRoute('app_dev_dashboard');
        }
        return $this->render('register_juego/index.html.twig', [
            'formulario' => $form->createView()
        ]);
    }
}
