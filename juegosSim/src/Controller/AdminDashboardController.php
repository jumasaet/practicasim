<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminDashboardController extends AbstractController
{
    #[Route('/admin/dashboard', name: 'app_admin_dashboard')]
    public function index(UserRepository $userRep): Response
    {
        //Buscamos los User que tengan los roles que queremos y los metemos a todos en una lista
        $usuarios = array();
        //El array_merge une los dos array que tendremos al usar findByUserRole()
        $usuarios = array_merge($userRep->findByUserRole(USER::ROLE_DEV),
                                $userRep->findByUserRole(USER::ROLE_QA));

        return $this->render('admin_dashboard/index.html.twig', [
            'usuarios' => $usuarios,
        ]);
    }

    #[Route('/admin/delete/{id}', name: 'admin_delete')]
    public function delete(User $user, ManagerRegistry $doctrine): Response
    {
        $em = $doctrine->getManager();
        $em->remove($user);
        $em->flush();
        return $this->redirectToRoute('app_admin_dashboard');
    }

    #[Route('/admin/alter/{id}', name: 'app_alterrol')]
    public function alterRol(User $user, ManagerRegistry $doctrine): Response
    {
        $em = $doctrine->getManager();
        if ($user->getTipo()=="Desarrollador")
        {
            $user->setRoles([User::ROLE_QA]);
        }else{
            $user->setRoles([User::ROLE_DEV]);
        }
        $em->persist($user);
        $em->flush();
        return $this->redirectToRoute('app_admin_dashboard');
    }
}
