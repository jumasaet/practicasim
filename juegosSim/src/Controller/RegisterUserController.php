<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrateUserType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class RegisterUserController extends AbstractController
{
    #[Route('/register/user', name: 'app_register_user')]
    public function index(Request $request , ManagerRegistry $doctrine, UserPasswordHasherInterface $endcoderPassword): Response
    {
        //se le asigna ese valor ya que es el rol por defecto y en el constructor que le hicimos pusimos que reciba un rol
        $user = new User(User::ROLE_QA,"Tester");
        $form = $this->createForm(RegistrateUserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $user = $form->getData();
            $user->setPassword($endcoderPassword->hashPassword($user,$form["password"]->getData()))    ;

            if ($this->isGranted(User::ROLE_ADMIN))
            {
                $user->setRoles([USER::ROLE_DEV]);
            }else
            {
                $user->setRoles([USER::ROLE_QA]);
            }

            $em = $doctrine->getManager();
            $em->persist($user);
            $em->flush();

            if ($this->isGranted(User::ROLE_ADMIN))
            {
                return $this->redirectToRoute('app_admin_dashboard');
            }else {
                return $this->redirectToRoute('app_login');
            }
        }
        return $this->render("register_user/index.html.twig",[
            'formulario' => $form->createView()
        ]);
    }
}
