<?php

namespace App\Controller;

use App\Repository\JuegoRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class QaDashboardController extends AbstractController
{
    #[Route('/qa/dashboard', name: 'app_qa_dashboard')]
    public function index( JuegoRepository $juegosRep, AuthenticationUtils $authenticationUtils,UserRepository $usRep): Response
    {
        $juegos = $juegosRep->findAll();
        $lasUser = $authenticationUtils->getLastUsername();
        $user = $usRep->findOneByUserMail($lasUser);
        return $this->render('dev_dashboard/index.html.twig', [
            'juegos' => $juegos,'usuario' => $user,
        ]);
    }
}
