<?php

namespace App\Entity;

use App\Repository\TestingRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TestingRepository::class)]
class Testing
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::ARRAY, nullable: true)]
    private array $idUser = [];

    #[ORM\Column]
    private ?int $idJuego = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdUser(): array
    {
        return $this->idUser;
    }

    public function setIdUser(?array $idUser): self
    {
        $this->idUser = $idUser;

        return $this;
    }

    public function getIdJuego(): ?int
    {
        return $this->idJuego;
    }

    public function setIdJuego(int $idJuego): self
    {
        $this->idJuego = $idJuego;

        return $this;
    }
}
